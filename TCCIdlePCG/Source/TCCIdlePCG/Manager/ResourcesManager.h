// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ResourcesManager.generated.h"

UENUM(BlueprintType)
enum class EResourceTypes : uint8
{
	RTECoin UMETA(DisplayName = "Coins"),
	RTEWood UMETA(DisplayName = "Wood"),
	RTEStone UMETA(DisplayName = "Stone"),
	RTEIron UMETA(DisplayName = "Iron")

};

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FUpdateResourceDelegate, EResourceTypes, Resource, float, Value );

UCLASS()
class TCCIDLEPCG_API AResourcesManager : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AResourcesManager();

	//UPROPERTY(BlueprintReadOnly, Category = "Resources");
	float coins, wood, stones, iron;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	FTimerHandle ResourceTimerHandle;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(BlueprintAssignable, Category = "Resources")
		FUpdateResourceDelegate OnResourceUpdatedDelegate;
	
	
};
